<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
class SummaryOforganization extends DB{
    public $id;
    public $org_name;
    public $org_summary;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("org_name",$postVariable)){
            $this->org_name=$postVariable['org_name'];
        }
        if(array_key_exists("org_summary",$postVariable)){
            $this->org_summary=$postVariable['org_summary'];
        }
    }

    public function store(){
        $arrData=array($this->org_name,$this->org_summary);
        $sql="insert into summary_of_organization(org_name,org_summary)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
    public function index(){
        $sql="SELECT * from summary_of_organization WHERE is_deleted='No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function view(){
        $sql="SELECT * from summary_of_organization where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $arrData=array($this->org_name,$this->org_summary);
        $sql="UPDATE summary_of_organization set org_name=?,org_summary=? WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("index.php");

    }

    public function delete(){
        $sql="DELETE from summary_of_organization WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trashed.php");
    }

    public function trash(){
        $sql="UPDATE summary_of_organization SET is_deleted=NOW() WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");

    }
    public function trash_list(){
        $sql="SELECT * from summary_of_organization WHERE is_deleted<>'No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE summary_of_organization SET is_deleted='No' WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");
    }
}
?>
