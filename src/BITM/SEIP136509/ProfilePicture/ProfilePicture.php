<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $profile_picture="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("profile_picture",$postVariable)){
            $this->profile_picture=$postVariable['profile_picture'];
        }
    }

    public function store(){
        $folder="/xampp/htdocs/LabExam8_AtomicProject_UmmeBatul_136509_b36/image/";
        $path=$folder.time().$_FILES['profile_picture']['name'];
        $temporary_location=$_FILES['profile_picture']['tmp_name'];
        $img=move_uploaded_file($temporary_location,$path);

        $arrData=array($this->name,$path);
        $sql="insert into profile_picture(name,profile_picture)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');



    }
    public function index(){
        $sql="SELECT * from profile_picture WHERE is_deleted='No' ORDER BY id DESC ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function view(){
        $sql="SELECT * from profile_picture where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $folder="/xampp/htdocs/LabExam8_AtomicProject_UmmeBatul_136509_b36/image/";
        $path=$folder.time().$_FILES['profile_picture']['name'];
        $temporary_location=$_FILES['profile_picture']['tmp_name'];
        $img=move_uploaded_file($temporary_location,$path);
        $arrData=array($this->name,$path);
        $sql="UPDATE profile_picture SET name=?,profile_picture=? WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("index.php");

    }

    public function delete(){
        $sql="DELETE from profile_picture WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trashed.php");
    }

    public function trash(){
        $sql="UPDATE profile_picture SET is_deleted=NOW() WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");

    }
    public function trash_list(){
        $sql="SELECT * from profile_picture WHERE is_deleted<>'No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE profile_picture SET is_deleted='No' WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");
    }


}

?>


