<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("hobby_name",$postVariable)){
            $this->hobbies=$postVariable['hobby_name'];
        }
    }

    public function store(){
        $chk=implode(",",$this->hobbies);
        for($i=0;$i<sizeof($chk);$i++);

       /* $checkbox1=$this->hobbies;
        $chk="";
        foreach($checkbox1 as $chk1){
            $chk.=$chk1." ";
        }*/

        $arrData=array($this->name,$chk);
        $sql="insert into hobbies(name,hobbies)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
    public function index(){
        $sql="SELECT * from hobbies WHERE is_deleted='No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function view(){
        $sql="SELECT * from hobbies where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $chk=implode(",",$this->hobbies);
        for($i=0;$i<sizeof($chk);$i++);
        $arrData=array($this->name,$chk);
        $sql="UPDATE hobbies set name=?,hobbies=? WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("index.php");

    }

    public function delete(){
        $sql="DELETE from hobbies WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trashed.php");
    }

    public function trash(){
        $sql="UPDATE hobbies SET is_deleted=NOW() WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");

    }
    public function trash_list(){
        $sql="SELECT * from hobbies WHERE is_deleted<>'No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE hobbies SET is_deleted='No' WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");
    }


}

?>

