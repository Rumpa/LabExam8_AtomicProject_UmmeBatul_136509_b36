<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
class Gender extends DB{
    public $id="";
    public $name="";
    public $gender="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("gender",$postVariable)){
            $this->gender=$postVariable['gender'];
        }
    }

    public function store(){
        $arrData=array($this->name,$this->gender);
        $sql="insert into gender(name,gender)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
    public function index(){
        $sql="SELECT * from gender WHERE is_deleted='No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function view(){
        $sql="SELECT * from gender where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $arrData=array($this->name,$this->gender);
        $sql="UPDATE gender set name=?,gender=? WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("index.php");

    }

    public function delete(){
        $sql="DELETE from gender WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trashed.php");
    }

    public function trash(){
        $sql="UPDATE gender SET is_deleted=NOW() WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");

    }
    public function trash_list(){
        $sql="SELECT * from gender WHERE is_deleted<>'No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE gender SET is_deleted='No' WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");
    }
}

?>

