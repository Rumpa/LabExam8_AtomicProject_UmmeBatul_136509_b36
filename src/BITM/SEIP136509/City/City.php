<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class City extends DB{
    public $id="";
    public $name="";
    public $city="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("city_name",$postVariable)){
            $this->city=$postVariable['city_name'];
        }
    }

    public function store(){
        $city=implode(',',$this->city);
        $arrData=array($this->name,$city);
        $sql="insert into city(name,city)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
    public function index(){
        $sql="SELECT * from city WHERE is_deleted='No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function view(){
        $sql="SELECT * from city where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $arrData=array($this->name,$this->city);
        $sql="UPDATE city set name=?,city=? WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("index.php");

    }

    public function delete(){
        $sql="DELETE from city WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trashed.php");
    }

    public function trash(){
        $sql="UPDATE city SET is_deleted=NOW() WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");

    }
    public function trash_list(){
        $sql="SELECT * from city WHERE is_deleted<>'No' ORDER BY id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE city SET is_deleted='No' WHERE id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("index.php");
    }


}

?>