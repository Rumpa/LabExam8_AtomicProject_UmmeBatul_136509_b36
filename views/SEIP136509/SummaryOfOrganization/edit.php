<?php
require_once("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOforganization;
$objSummary=new SummaryOforganization();
$objSummary->setData($_GET);
$oneData=$objSummary->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <link rel="icon" href="../../../resource/assets/images/favicon.ico">

    <title>Atomic Project</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="../../../resource//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-core.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-theme.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-forms.css">
    <link rel="stylesheet" href="../../../resource/assets/css/custom.css">
    <link rel="stylesheet" href="../../../resource/assets/css/skins/blue.css">
    <link rel="stylesheet" href="../../../resource/assets/css/font-icons/font-awesome/css/font-awesome.min.css">

    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/assets/js/jquery-1.11.3.min.js"></script>
    <script src="../../../resource/assets/js/bootstrap.min.js"

    <!--[if lt IE 9]><script src="../../../resource/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body skin-blue" data-url="http://neon.dev">
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <?php include_once('../../../sidebar_menu.php') ?>


    <div class="main-content">



        <hr />

        <ol class="breadcrumb bc-3" >
            <li>
                <a href="index.php"><i class="fa fa-home"></i>Home</a>
            </li>
            <li>

                <a href="forms-main.html">Forms</a>
            </li>
            <li class="active">

                <strong>Basic Elements</strong>
            </li>
        </ol>


        <h2>Edit Summary of an Organization </h2>
        <br />
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Edit Organization Name and Summary:
                        </div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">


                        <form role="form" action="update.php" method="post" class="form-horizontal form-groups-bordered" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $oneData->id  ?>" >

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Organization Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="org_name" value="<?php echo $oneData->org_name  ?>" class="form-control" id="field-1" placeholder="Enter Your Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-2" class="col-sm-3 control-label">Organization Summary</label>
                                <div class="col-sm-5">
                                    <textarea class="form-control"  name="org_summary" rows="5" id="field-2"><?php echo $oneData->org_summary ?></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-blue">Update</button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="resource/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="resource/assets/js/rickshaw/rickshaw.min.css">

    <!-- Bottom scripts (common) -->
    <script src="resource/assets/js/gsap/TweenMax.min.js"></script>
    <script src="resource/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="resource/assets/js/bootstrap.js"></script>
    <script src="resource/assets/js/joinable.js"></script>
    <script src="resource/assets/js/resizeable.js"></script>
    <script src="resource/assets/js/neon-api.js"></script>
    <script src="resource/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>


    <!-- Imported scripts on this page -->
    <script src="resource/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
    <script src="resource/assets/js/jquery.sparkline.min.js"></script>
    <script src="resource/assets/js/rickshaw/vendor/d3.v3.js"></script>
    <script src="resource/assets/js/rickshaw/rickshaw.min.js"></script>
    <script src="resource/assets/js/raphael-min.js"></script>
    <script src="resource/assets/js/morris.min.js"></script>
    <script src="resource/assets/js/toastr.js"></script>
    <script src="resource/assets/js/neon-chat.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="resource/assets/js/neon-custom.js"></script>


    <!-- Demo Settings -->
    <script src="resource/assets/js/neon-demo.js"></script>

</body>
</html>















<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Summary of Organization</title>
    <script src="../../../resource/assets/js/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#demo").delay(2000).fadeOut("slow")
        });

    </script>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href=/"assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>
<p id="demo">
    <?php

    use App\Message\Message;
    echo Message::message();
    ?>
</p>
<!-- Top content -->
<div class="top-content">


    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Edit Organization Summary</h3>
                            <p>Edit Organization name and Summary:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-institution"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="update.php" method="post" class="login-form">
                            <input type="hidden" name="id" value="<?php echo $oneData->id  ?>" >
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Organization Name</label>
                                <input type="text" name="org_name"  value="<?php echo $oneData->org_name  ?>" class="form-username form-control" id="org_name">
                            </div>
                            <div class="form-group">
                                <label for="org_summary">Organization Summary:</label>
                                <textarea class="form-control" name="org_summary" rows="5" id="org_summary"><?php echo $oneData->org_summary ?></textarea>
                            </div>
                            <button type="submit" class="btn">Update</button>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>


<!-- Javascript -->

<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>