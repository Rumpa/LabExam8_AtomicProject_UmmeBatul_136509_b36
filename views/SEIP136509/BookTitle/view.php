<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="../../../resource/assets/css/style.css">-->
    <link rel="icon" href="../../../resource/assets/images/favicon.ico">
    <title>Atomic Project</title>
    <script src="../../../resource/assets/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="../../../resource/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="../../../resource//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-core.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-theme.css">
    <link rel="stylesheet" href="../../../resource/assets/css/neon-forms.css">
    <link rel="stylesheet" href="../../../resource/assets/css/custom.css">
    <link rel="stylesheet" href="../../../resource/assets/css/skins/blue.css">
    <link rel="stylesheet" href="../../../resource/assets/css/font-icons/font-awesome/css/font-awesome.min.css">

    <!--[if lt IE 9]><script src="../../../resource/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../../https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="../../../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-body skin-blue" data-url="http://neon.dev">
<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
$objBookTitle=new BookTitle();
$objBookTitle->setData($_GET);
$oneData=$objBookTitle->view();
?>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <?php include_once('../../../sidebar_menu.php'); ?>

    <div class="main-content">



        <hr />

        <ol class="breadcrumb bc-3" >
            <li>
                <a href="index.php"><i class="fa fa-home"></i>Home</a>
            </li>
            <li>

                <a href="forms-main.html">Forms</a>
            </li>
            <li class="active">

                <strong>Basic Elements</strong>
            </li>
        </ol>


        <h2>Book Title</h2>

        <br />
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>View</h4>
                        </div>
                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table id="mytable" class="table table-bordred table-striped">
                                            <thead>
                                            <th>ID</th>
                                            <th>Book Title</th>
                                            <th>Author Name</th>
                                            </thead>
                                            <tbody>

                                                <tr>

                                                    <td><?php echo $oneData->id; ?></td>
                                                    <td><?php echo $oneData->book_title; ?></td>
                                                    <td><?php echo $oneData->author_name; ?></td>
                                                    <td>

                                                </tr>
                                            </tbody>


                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>






                    </div>

                </div>

            </div>
        </div>

    </div>

    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="../../../resource/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="../../../resource/assets/js/rickshaw/rickshaw.min.css">

    <!-- Bottom scripts (common) -->
    <script src="../../../resource/assets/js/gsap/TweenMax.min.js"></script>
    <script src="../../../resource/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="../../../resource/assets/js/bootstrap.js"></script>
    <script src="../../../resource/assets/js/joinable.js"></script>
    <script src="../../../resource/assets/js/resizeable.js"></script>
    <script src="../../../resource/assets/js/neon-api.js"></script>
    <script src="../../../resource/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>


    <!-- Imported scripts on this page -->
    <script src="../../../resource/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
    <script src="../../../resource/assets/js/jquery.sparkline.min.js"></script>
    <script src="../../../resource/assets/js/rickshaw/vendor/d3.v3.js"></script>
    <script src="../../../resource/assets/js/rickshaw/rickshaw.min.js"></script>
    <script src="../../../resource/assets/js/raphael-min.js"></script>
    <script src="../../../resource/assets/js/morris.min.js"></script>
    <script src="../../../resource/assets/js/toastr.js"></script>
    <script src="../../../resource/assets/js/neon-chat.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="../../../resource/assets/js/neon-custom.js"></script>


    <!-- Demo Settings -->
    <script src="../../../resource/assets/js/neon-demo.js"></script>

</body>
</html>















