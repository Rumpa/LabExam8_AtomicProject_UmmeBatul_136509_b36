-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 10:12 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `is_deleted`) VALUES
(31, 'Irrin', '2016-11-09', 'No'),
(36, 'Ruksana', '2015-10-20', '2016-11-21 00:06:56'),
(37, 'Nasima', '2016-11-30', 'No'),
(38, 'Siam Ali', '2016-11-30', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(20) NOT NULL,
  `book_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(10, 'MS Excel', 'Sumon Ahmed', 'No'),
(11, 'MS Powerpoint', 'Sumon Ahmed', '2016-11-21 01:06:05'),
(12, 'sima', 'simubb', 'No'),
(13, 'Nilpori', 'Humayun Ahmed', 'No'),
(14, 'Dipu no 2', 'Jafar Iqbal', '2016-11-21 01:05:09'),
(15, 'Oracle Learning', 'Mahfuz Ahmed', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_deleted`) VALUES
(6, 'Sarmin Ahmed', 'Chittagong', 'No'),
(7, 'Anil Barua', 'Barisal', '2016-11-20 13:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_deleted`) VALUES
(1, 'Nitul', 'nitul@yahoo.com', 'No'),
(2, 'Salma', 'salma@gmail.com', '2016-11-20 11:34:56'),
(4, 'habib', 'habib@gmail.com', '2016-11-20 00:22:46'),
(6, 'Sumona', 'sumona@gmail.com', '2016-11-20 00:22:50'),
(7, 'Sujon Ahmed', 'sujon@gmail.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_deleted`) VALUES
(1, 'Rumpa', 'Female', '2016-11-20 00:41:47'),
(3, 'Batul', 'Female', 'No'),
(4, 'Habib', 'Male', 'No'),
(8, 'Jalal Ahmed', 'Male', 'No'),
(9, 'Selim', 'Male', 'No'),
(11, 'Kamal', 'Male', '2016-11-20 11:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobbies` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_deleted`) VALUES
(1, 'Rumpa', 'Cooking,Travelling', '2016-11-20 00:43:14'),
(5, 'Aiman', 'Travelling,Photography,Cooking', '2016-11-20 13:43:56'),
(6, 'Sujon Ahmed', 'Travelling,Photography', 'No'),
(7, 'Jahanara', 'Travelling,Photography,Cooking', 'No'),
(8, 'Sayeem', 'Travelling,Cycling', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_deleted`) VALUES
(11, 'Habib', '/xampp/htdocs/LabExam8_Atomic_Project_UmmeBatul_136509_b36/image/1479627098Jellyfish.jpg', '2016-11-19 23:40:37'),
(13, 'Saima Noor', '/xampp/htdocs/LabExam8_Atomic_Project_UmmeBatul_136509_b36/image/1479627536Tulips.jpg', '2016-11-19 23:40:41'),
(14, 'Jainab', '/xampp/htdocs/LabExam8_Atomic_Project_UmmeBatul_136509_b36/image/1479670981Lighthouse.jpg', 'No'),
(15, 'Saima Ahmed', '/xampp/htdocs/LabExam8_Atomic_Project_UmmeBatul_136509_b36/image/1479673423Chrysanthemum.jpg', 'No'),
(16, 'Sajjad', '/xampp/htdocs/LabExam8_Atomic_Project_UmmeBatul_136509_b36/image/1479671762Koala.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(20) NOT NULL,
  `org_name` varchar(100) NOT NULL,
  `org_summary` varchar(100) NOT NULL,
  `is_deleted` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `org_name`, `org_summary`, `is_deleted`) VALUES
(1, 'BITM', 'BASIS Institute of Technology & Management', '2016-11-20 13:52:44'),
(2, 'New Horizon', 'Is a institute of It', 'No'),
(5, 'Belancer', 'Institution of ', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
